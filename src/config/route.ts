// 定义一些路由 每个路由都需要映射到一个组件
import IndexPage from "../pages/home/Index.vue";
import TeamPage from "../pages/teams/TeamPage.vue";
import UserPage from "../pages/user/UserPage.vue";
import SearchPage from "../pages/home/SearchPage.vue";
import UserEditPage from "../pages/user/UserEditPage.vue";
import SearchResultPage from "../pages/home/SearchResultPage.vue";
import UserLoginPage from "../pages/user/UserLoginPage.vue";
import TeamAddPage from "../pages/teams/TeamAddPage.vue";
import TeamUpdatePage from "../pages/teams/TeamUpdatePage.vue";
import TeamInfoPage from "../pages/teams/TeamInfoPage.vue";
import UserUpdatePage from "../pages/user/UserUpdatePage.vue";
import UserTeamJoinPage from "../pages/teams/UserTeamJoinPage.vue";
import UserTeamCreatePage from "../pages/teams/UserTeamCreatePage.vue";
import UserInfoPage from "../pages/user/UserInfoPage.vue";

const routes = [
    { path: '/', component: IndexPage },
    { path: '/team', title: '队伍大厅', component: TeamPage },
    { path: '/user', title: '个人信息', component: UserPage },
    { path: '/search', title: '码农筛选', component: SearchPage },
    { path: '/user/edit', title: '编辑信息', component: UserEditPage },
    { path: '/user/list', title: '用户列表', component: SearchResultPage },
    { path: '/user/login', title: '登录', component: UserLoginPage },
    { path: '/team/add', title: '创建队伍', component: TeamAddPage },
    { path: '/team/update', title: '更新队伍', component: TeamUpdatePage },
    { path: '/team/info', title: '队伍信息', component: TeamInfoPage },
    { path: '/user/update', title: '更新信息', component: UserUpdatePage },
    { path: '/user/team/join', title: '加入队伍', component: UserTeamJoinPage },
    { path: '/user/team/create', title: '创建队伍', component: UserTeamCreatePage },
    { path: '/user/info', title: '个人信息页', component: UserInfoPage },
]

export default routes;